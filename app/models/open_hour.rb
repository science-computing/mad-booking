# frozen_string_literal: true

# OpenHour represents a schedule during which the service is open.
# It consists of a boolean value for each day of the week, indicating
# if the service is open on that day.
# It also has a start and end time, which are the hours that the service is
# open during the days defined as open.
# Lastly, it has a valid from/until range. These are the dates during which the
# schedule is valid.
class OpenHour < ActiveRecord::Base
  validate :correct_times

  # given a date, return an array of open times with this format:
  # [[start,end],[start,end],...,[start,end]]
  def self.open_times_from_date(test_date)
    open_times = []

    OpenHour.all.each do |oh|
      if test_date.between?(oh.valid_from, oh.valid_until) &&
         oh.send(Date::DAYNAMES[test_date.wday].downcase)
        open_times.push([oh.start_time, oh.end_time])
      end
    end

    open_times
  end

  private

  def correct_times
    # Is the start date before the end date
    valid_dates = valid_from <= valid_until

    # is the start time before the end time?
    valid_times = start_time < end_time

    valid = valid_times && valid_dates
    return if valid

    # If the times aren't valid, return the error
    errors.add(
      :open_hour,
      'cannot be saved: Start time must come before end time'
    )
  end
end
