# frozen_string_literal: true

# Booking model describes a single equipment booking. This booking may be
# composed of multiple Events. A booking has a related list of events,
# a schedule that describes the recurrence pattern of the events (which is nil)
# for bookings with a single event, equipment, the most recent sign in and
# sign out times for the equipment, and an associated user

# The methods attached to the model generate a short description of the booking,
# get the status of a piece of equipment on the booking, and do some validation
# of the user and the times associated with the booking
class Booking < ActiveRecord::Base
  include IceCube
  include ActiveModel::Validations

  belongs_to :user
  belongs_to :approver, class_name: 'User', inverse_of: :approved_by, optional: true
  has_many :events, dependent: :destroy
  has_and_belongs_to_many :equipment
  serialize :sign_in_times, Hash
  serialize :sign_out_times, Hash
  serialize :schedule
  validate :real_user
  validate :allowed_user
  validate :correct_times
  validate :has_no_conflicts
  validate :events_while_open
  validate :not_selfserve_past
  validates_presence_of :equipment

  # validates_associated :events

  @@valid_statuses = [['Pending', 0], ['Approved', 1], ['Rejected', 2]]

  # Give a short description of the booking: Start date/time, end date/time, user, recurring?
  def short_description
    desc = format('<br /><i>ID</i>: %s<br />', self.id)
    desc += format('<i>Start Date</i>: %s %s<br />', events[0].start.localtime.strftime('%D'), events[0].start.localtime.strftime('%r'))
    desc += format('<i>End Date</i>: %s %s<br />', events[0].end.localtime.strftime('%D'), events[0].end.localtime.strftime('%r'))
    desc += format('<i>Username</i>: %s<br />', user.username)
    desc += '<i>Recurring</i><br />' if schedule?
    desc += format('<i>Equipment</i>: %s<br />', equipment.map(&:name).join(','))
    desc
  end

  def self_serve_description
    desc = format('<br /><i>Start Date</i>: %s %s<br />', events[0].start.localtime.strftime('%D'), events[0].start.localtime.strftime('%r'))
    desc += format('<i>End Date</i>: %s %s<br />', events[0].end.localtime.strftime('%D'), events[0].end.localtime.strftime('%r'))
    desc += '<i>Recurring</i><br />' if schedule?
    desc += format('<i>Equipment</i>: %s<br />', equipment.map(&:name).join(','))
    desc
  end

  # getter for the valid_statuses array
  def self.valid_statuses
    @@valid_statuses
  end

  # returns the status string by the status number
  def self.get_status(status_id)
    @@valid_statuses.each do |status|
      return status[0] if status[1] == status_id
    end
    'Invalid Status'
  end

  # return booking active status
  def get_active_status_colour
    recent_event = get_most_recent_event
    num_items_in = 0
    b_colour = "#d8b3e3" # purple
    equipment.each do |e|
      item_status = get_item_status(e.id)
      if item_status == 'Out'
        # If at least 1 item is out the booking is active
        b_colour = "#76bcc4" # blue
      elsif item_status == 'Overdue'
        # If at least 1 item is overdue, the booking is overdue
        b_colour = "#ff7c7c" # red
      elsif item_status == 'In'
        # All items need to be in for the booking to be finished
        num_items_in += 1
      end
    end

    # If number of items In matches number of items in booking, it is
    # finished if non-recurring or if it is recurring and the last event
    # has ended
    if (num_items_in == equipment.length) &&
       (schedule.nil? || events.last.end <= Time.now)
      b_colour = '#b2e593' # green
    end

    # If event is pending, override the colour to grey
    if approval_status == 0
      b_colour = '#d2d2d2' # grey
    elsif approval_status == 2
      b_colour = '#ff4500' # orangered
    end
    b_colour
  end

  # Return the event that is either current, or the one with the most recent
  # end time
  def get_most_recent_event
    # get all events sorted by start date, most recent first
    events_desc = events.sort_by(&:start).reverse
    # for each event, working backwards in time, we want to know if it is the:
    # "current" event (now b/w start and end). If it is, return it.
    #
    # If it isn't, we want the most recent event in the past
    most_recent_event = nil

    events_desc.each do |ev|
      if ev.current?
        return ev
      elsif ev.past?
        return ev
      end
    end
  end

  # Return true if a booking has equipment out, but not in or overdue
  def is_active?
    statuses = equipment.map { |e| get_item_status(e.id) }
    if statuses.include?('Out') && !statuses.include?('Overdue')
      true
    else
      false
    end
  end

  # Return true if a booking has at least one piece of overdue equipment
  def is_overdue?
    statuses = equipment.map { |e| get_item_status(e.id) }
    if statuses.include?('Overdue')
      true
    else
      false
    end
  end

  # Return one of "Booked, Out, In, Overdue" for a piece of equipment
  # identified with item_id
  def get_item_status(item_id)
    sit_key = sign_in_times.key?(item_id)
    sot_key = sign_out_times.key?(item_id)
    status = ''

    # If there are no sign in or sign out times for the item, it is BOOKED
    if sit_key == false && sot_key == false
      status = 'Booked'
      status
    # The item has been signed out, but not in
    elsif sot_key == true
      # The item has never been signed in
      if sit_key == false
        recent_ev = get_most_recent_event
        recent_ev = recent_ev.first if recent_ev.is_a?(Array)
        # If due before now, it is out. If due after now, it is overdue.
        # For recurring events, I need the most "recent" event
        status = if recent_ev.current? || recent_ev.future?
                   'Out'
                 else
                   'Overdue'
                 end
      # The item has previously been signed in
      else
        # For one-time bookings, it is in
        status = if !schedule?
                   'In'
                 # For recurring bookings it goes back to "Booked"
                 else
                   'Booked'
                 end
      end
    end
  end

  def self_serve_editable(username)
    # a self serve booking shouldn't be editable by a non-admin if any of the following are true:
    # 1) Booking start time has passed
    # 2) Anything has been signed out
    # 3) Booking has been approved
    # 4) Booking has been rejected

    editable_status = true
    if User.find_by_username(username).admin
      # If logged in user is an admin, they can edit anything
      editable_status = true
    else
      # logged in user is not an admin, they can only edit their own bookings
      # under certain conditions:
      # 1) booking is self-serve
      # 2) booking is in the future
      # 3) booking has not been approved or rejected
      # 4) nothing has been signed out
      if self_serve? && user.username == username && get_most_recent_event[0].start > DateTime.now && approval_status == 0 && !is_active? && !is_overdue?
        editable_status = true
      else
        editable_status = false
      end
    end
    editable_status
  end

  private

  def real_user
    # Check to see if the user already exists. If not fail the validation.
    valid = User.exists?(user_id)
    unless valid
      errors.add(:user, "doesn't exist. Please create the user first.")
    end
  end

  def allowed_user
    # Check to see that the user is allowed to book equipment.
    # status = 0 - user is in good standing
    # status = 1 - user is greylisted
    # status = 2 - user is blacklisted

    if !user_id.nil?
      status = User.find(user_id).status
    else
      errors.add(:user, 'does not exist.')
    end

    return nil if status != 2

    errors.add(:user, 'has been blacklisted and is not allowed to book equipment.')
  end

  def correct_times
    return nil if events.empty?

    # Check to see that the times for the booking are sensible

    # Is the start date/time before the end date/time?
    @e = events.first
    valid = @e.start < @e.end
    unless valid
      errors.add(:booking, 'cannot be saved: Start time must come before end time')
    end
  end

  def not_selfserve_past
    test_event = events.first

    # If this is a self serve booking, it must be for a date/time in the future
    # Events created by an admin always pass this validation
    valid = if self_serve?
              test_event.start > Time.now && test_event.end > Time.now
            else
              true
            end
    unless valid
      errors.add(:booking, 'cannot be saved: Self serve bookings in the past are not valid')
    end
  end

  def has_no_conflicts
    # Find bookings where the times are overlapping and the equipment matches

    equip_list = equipment_ids
    conflict_bookings = []
    # For each event associated with the booking, check for conflicts
    events.each do |ev|
      test_start_time = ev.start.utc.strftime('%Y-%m-%d %H:%M:%S')
      test_end_time = ev.end.utc.strftime('%Y-%m-%d %H:%M:%S')
      conflicts = check_for_conflicts(test_start_time, test_end_time, equip_list)
      conflict_bookings.push(conflicts) unless conflicts.empty?
    end

    # If there is a conflict, add an error for it
    conflict_bookings.flatten.each do |cb|
      errors.add(:booking, "conflicts found with #{cb.short_description}")
    end
  end

  def check_for_conflicts(test_start_time, test_end_time, equip_list)
    bookings = Booking.joins(:events).joins(:equipment)
                      .where(
                        '"end" BETWEEN ? AND ?', test_start_time, test_end_time
                      )
                      .or(
                        Booking.joins(:events).joins(:equipment).where('start BETWEEN ? AND ?', test_start_time, test_end_time)
                      )
                      .or(
                        Booking.joins(:events).joins(:equipment).where('start <= ? AND ? < "end"', test_start_time, test_end_time)
                      )
                      .where(
                        "bookings_equipment.equipment_id": equip_list
                      )
                      .where.not(
                        "bookings.id": id
                      )
    # Get bookings marked as conflicted and test to see if they are complete
    # or not.
    bookings_array = bookings.to_a
    conflicted_bookings = []
    bookings_array.each do |booking|
      statuses = equip_list.map { |eid| booking.get_item_status(eid) }
      # Bookings shouldn't conflict if all of their items are "In"

      unless (statuses.all? { |s| s == 'In' }) || (booking.approval_status == 2)
        conflicted_bookings.push(booking)
      end
    end
    conflicted_bookings
  end

  def events_while_open
    open = true
    events.each do |e|
      unless e.valid_open_close?
        open = false
        break
      end
    end

    unless open
      errors.add(:booking, format('Not open at start and/or end time of booking. Details: %s', short_description))
    end
  end
end
