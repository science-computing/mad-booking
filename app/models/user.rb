# frozen_string_literal: true

require 'json'
require 'net/ldap'
# Users serve two purposes in the application. They are the admins managing
# bookings and other content, and they are the people who are reserving
# equipment for their use. Those with the ability to manage all bookings are
# classified as admins (User.admin == true). Those who are simply "clients"
# are not (User.admin == false).
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # devise :database_authenticatable, :registerable,
  #        :recoverable, :rememberable, :trackable, :validatable
  # devise :cas_authenticatable
  # include HTTParty

  before_save :default_values
  has_many :bookings
  has_many :approved_by, class_name: 'Booking', inverse_of: :approver
  accepts_nested_attributes_for :bookings
  validates :username, uniqueness: true
  validates :username, presence: true

  @@valid_statuses = [['Good Standing', 0], ['Warned', 1], ['Blacklisted', 2]]

  def self.find_or_create_from_username(username)
    u = User.find_by_username(username)
    u = User.create(username: username) if u.nil?
    u
  end

  def self.admin_user?(uname)
    admin_user = false

    unless User.where(username: uname).empty?
      admin_user = !User.where(username: uname, admin: true).empty?
    end

    admin_user
  end

  def default_values
    self.admin = false if admin.nil?
    self.status = 0 if status.nil?
  end

  # Is the user from a valid department?

  # getter for the valid_statuses array
  def self.valid_statuses
    @@valid_statuses
  end

  # returns the status string by the status number
  def self.get_status(status_id)
    @@valid_statuses.each do |status|
      return status[0] if status[1] == status_id
    end

    'Invalid Status'
  end


  def self.ldap_search(username_part)
    # Create ldap connection
    ldap = Net::LDAP.new(
      host: 'uwldap.uwaterloo.ca',
      base: 'dc=uwaterloo,dc=ca'
    )

    # Search for username
    search_result = ldap.search(filter: Net::LDAP::Filter.construct("
      (&(&(ou=*)(!(ou=Unknown)))(uid=#{username_part}*))"))

    if search_result.nil?
      {}
    else
      search_result
    end
  end

  # TODO: Replace with LDAP query
  # returns whether a username is a known LDAP user
  def self.ldap_user?(username)
    collection = Collection.first
    # validate WatIAM by default, unless setting exists and is set to 0
    return true if collection.settings(:validation).watiam == '0'

    # Look for user in LDAP. If there is only one match for the username, and
    # it is an exact match for the UID in LDAP, return true
    if ldap_search(username).length == 1 && ldap_search(username)[0][:uid][0]
      return true 
    else
      # Not a valid LDAP user    
      return false
    end
  end

  def ldap_details
    # Create ldap connection
    ldap = Net::LDAP.new(
      host: 'uwldap.uwaterloo.ca',
      base: 'dc=uwaterloo,dc=ca'
    )

    # Search for username
    search_result = ldap.search(filter: Net::LDAP::Filter.eq('uid', username))

    if search_result.nil?
      {}
    else
      search_result.first
    end
  end

  # return true if the user is from a valid department (as set in Settings)
  # or false otherwise
  def validate_department
    collection = Collection.first
    ldap_response = User.ldap_search(username)
    if ldap_response.length == 1 && ldap_response[0][:uid][0]
      dept = ldap_response[0][:ou]
    end

    valid_depts = collection.settings(:validation).departments
    valid_depts_list =  if valid_depts.nil? or valid_depts.empty?
                          []
                        else
                          valid_depts.split('|').map(&:strip)
                        end
    # check for intersection between valid_depts_list and dept
    common_ou = valid_depts_list & dept
    if common_ou.length.positive?
      true
    else
      false
    end
  end

  # returns the user's email address from UW LDAP
  def email_address
    ldap_response = User.ldap_search(username)
    if ldap_response.length == 1 && ldap_response[0][:uid][0]
      # email = ldap_response[0][:mail]
      uid = ldap_response[0]["uid"][0]
      email = "#{uid}@uwaterloo.ca"
      return email
    else
      return nil
    end
  end
end
