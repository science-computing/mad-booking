# frozen_string_literal: true

# Events are the individual calendar items associated with a booking.
# Events should always have a parent booking. A booking can contain one
# or more events.
class Event < ActiveRecord::Base
  belongs_to :booking

  def valid_open_close?
    ok_open = during_open_range?
    ok_day_time = valid_day_time?
    ok_closed = !during_closed_range?
    (ok_open && ok_day_time && ok_closed)
  end

  # Check if an event is in an open range (1A)
  def during_open_range?
    ok_ranges_s = open_ranges_start
    ok_ranges_e = open_ranges_end
    !ok_ranges_s.empty? && !ok_ranges_e.empty? ? true : false
  end

  # Check that an event start and end does not fall in closed hour range
  # Return true if during a closed range
  def during_closed_range?
    start_range = ClosedHour.where(
      '? >= closed_start AND ? <= closed_end',
      start,
      start
    )
    end_range = ClosedHour.where(
      '? >= closed_start AND ? <= closed_end',
      self.end,
      self.end
    )

    (!start_range.empty? || !end_range.empty?)
  end

  # Return the OpenHour ranges where the start time of the event is after the
  # from time, and before the until time
  def open_ranges_start
    OpenHour.where('? >= valid_from AND ? <= valid_until', start, start)
  end

  # Return the OpenHour ranges where the end time of the event is after the
  # from time, and before the until time
  def open_ranges_end
    OpenHour.where('? >= valid_from AND ? <= valid_until', self.end, self.end)
  end

  # For all open ranges relevant to this event,
  # check if event matches day of week and time
  def valid_day_time?
    start_valid = false
    end_valid = false

    event_day_s = Date::DAYNAMES[start.wday].downcase
    ok_ranges_s = open_ranges_start
    ok_ranges_s.each do |open_range|
      ok_day_s = open_range.send(event_day_s)
      ok_times_s = start.to_datetime.localtime.strftime('%H%M')
        .between?(
          open_range.start_time.localtime.strftime('%H%M'),
          open_range.end_time.localtime.strftime('%H%M')
        )
      if ok_day_s & ok_times_s
        start_valid = true
        break
      end
    end

    event_day_e = Date::DAYNAMES[self.end.wday].downcase
    ok_ranges_e = open_ranges_end
    ok_ranges_e.each do |open_range|
      ok_day_e = open_range.send(event_day_e)
      ok_times_e = self.end.to_datetime.localtime.strftime('%H%M')
        .between?(
          open_range.start_time.localtime.strftime('%H%M'),
          open_range.end_time.localtime.strftime('%H%M')
        )
      if ok_day_e & ok_times_e
        end_valid = true
        break
      end
    end
    # if start and end event times both fall in valid ranges return true
    # otherwise return false
    start_valid && end_valid
  end

  # Need an is_current and is_past  and is_future function
  def current?
    (start <= Time.now) && (Time.now <= self.end)
  end

  def past?
    Time.now > self.end
  end

  def future?
    Time.now < start
  end
end
