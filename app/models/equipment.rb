# frozen_string_literal: true

# Equipment are the items that are loaned in/out by the application.
class Equipment < ActiveRecord::Base
  include PgSearch::Model
  pg_search_scope :search_equipment, against: %i[barcode name], using: {
    tsearch: { prefix: true }
  }

  before_save :default_values
  has_and_belongs_to_many :bookings
  has_and_belongs_to_many :tags
  validates_presence_of :name
  validates_uniqueness_of :barcode

  # the way the items in the array of arrays is listed is the way it
  # appears on the dropdown
  @@valid_statuses = [['In', 1], ['Out', 0], ['Out for Repair', 2]]

  # enforce a default value of 1 if no status value exists
  def default_values
    self.status = 1 if status.nil?
  end

  def related_events
    Event.joins(
      booking: :equipment
    )
         .where("bookings_equipment.equipment_id": id)
  end

  # Return a space-delimited list of tag ids associated with the item
  def tag_ids_stringlist
    # tag_ids = []
    # self.tags.each do |tag|
    #   tag_ids.push(tag.id)
    # end
    # tag_ids.join(" ")
    tag_ids.join(' ')
  end

  # getter for the valid_statuses array
  def self.valid_statuses
    @@valid_statuses
  end

  # returns the status string by the status number
  def self.get_status(status_id)
    @@valid_statuses.each do |status|
      return status[0] if status[1] == status_id
    end
    'Invalid Status'
  end
end
