# frozen_string_literal: true

# ClosedHour represents a range of time when the service is closed.
# It consists of a start_date_time and end_date_time. These override
# OpenHour definitions.
class ClosedHour < ActiveRecord::Base
  validates :closed_start, presence: true
  validates :closed_end, presence: true
  validate :correct_times

  # given a date, return an array of closed times with this format:
  # [[start,end],[start,end],...,[start,end]]
  def self.closed_times_from_date(test_date)
    closed_times = []
    ClosedHour.all.each do |ch|
      next unless test_date.to_date.between?(
        ch.closed_start.to_date,
        ch.closed_end.to_date
      )

      closed_times.push([ch.closed_start, ch.closed_end])
    end
    closed_times
  end

  private

  def correct_times
    valid = false
    valid = closed_start < closed_end if !closed_start.nil? && !closed_end.nil?
    return if valid

    # If the times are not valid, add an error
    errors.add(
      :closed_hour,
      'cannot be saved: Start time must come before end time'
    )
  end
end
