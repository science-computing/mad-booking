# frozen_string_literal: true

# Tags are used to help categorize equipment. A piece of equipment can have 0
# or more tags. Mostly used for filtering views where the list of equipment
# is too long.
class Tag < ActiveRecord::Base
  has_and_belongs_to_many :equipment
  validates :name, uniqueness: true
  validates :name, presence: true
end
