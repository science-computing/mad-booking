import axios from 'axios';

import Vue from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  new Vue({
    el: '#app',
    data () {
      return {}
    }
  })

  //Sidebar
  var sidebarToggleables= document.querySelectorAll('a[data-toggle="collapse"]')
  .forEach(st => {
    st.addEventListener('click', function(event){
      if (this.classList.contains('collapsed')){
        this.querySelector('svg[data-icon="caret-down"]').setAttribute("data-fa-transform", "rotate--90");
      }
      else {
        this.querySelector('svg[data-icon="caret-down"]').setAttribute("data-fa-transform", "rotate-0");
      }
    });
  });

  var sidebar = new Vue({
    el: '#sidebar',
    data: {
      notification_count: 0
    },
    mounted() {
      var self = this;

      axios.get("/bookings/overdue")
      .then(function(response){
        self.notification_count += response.data.length;
      });

      axios.get("/bookings/approvals")
      .then(function(response){
        self.notification_count = response.data.length;
      });
    }
  });


})