import SignInOut from './SignInOut.vue';
import Vue from 'vue';

document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    render: h => h(SignInOut)
  }).$mount('#sign_in_out');
});