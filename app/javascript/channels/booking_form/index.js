import BookingForm from './BookingForm.vue';
import Vue from 'vue';

document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    render: h => h(BookingForm)
  }).$mount('#booking_form');
});