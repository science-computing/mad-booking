import Calendar from './Calendar.vue';
import Vue from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  new Vue({
    render: h => h(Calendar)
  }).$mount('#calendar');

});