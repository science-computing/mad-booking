import Dashboard from './Dashboard.vue';
import Vue from 'vue';

document.addEventListener('DOMContentLoaded', () => {

  new Vue({
    render: h => h(Dashboard)
  }).$mount('#dashboard');

});