# frozen_string_literal: true

class OpenHoursController < ApplicationController
  before_action :set_open_hour, only: %i[show edit update destroy]

  before_action :authenticate
  before_action :authenticate_admin

  # GET /open_hours
  def index
    @open_hours = OpenHour.all
  end

  # GET /open_hours/1
  def show; end

  # GET /open_hours/new
  def new
    @open_hour = OpenHour.new
  end

  # GET /open_hours/1/edit
  def edit; end

  # POST /open_hours
  def create
    @open_hour = OpenHour.new(open_hour_params)
    # st = Time.strptime(open_hour_params[:start_time], "%k:%M")
    # et = Time.strptime(open_hour_params[:end_time], "%k:%M")

    # @open_hour.start_time = convert_to_utc(st.hour, st.min)
    # @open_hour.end_time = convert_to_utc(et.hour, et.min)
    @open_hour.start_time = Time.zone.parse(open_hour_params[:start_time]).utc
    @open_hour.end_time = Time.zone.parse(open_hour_params[:end_time]).utc

    if @open_hour.save
      redirect_to open_hours_path, notice: 'Open hour was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /open_hours/1
  def update
    # st = Time.strptime(open_hour_params[:start_time], "%k:%M")
    # et = Time.strptime(open_hour_params[:end_time], "%k:%M")
    
    @open_hour.attributes = open_hour_params
    # @open_hour.start_time = convert_to_utc(st.hour, st.min)
    # @open_hour.end_time = convert_to_utc(et.hour, et.min)
    @open_hour.start_time = Time.zone.parse(open_hour_params[:start_time]).utc
    @open_hour.end_time = Time.zone.parse(open_hour_params[:end_time]).utc

    if @open_hour.save
      redirect_to open_hours_path, notice: 'Open hour was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /open_hours/1
  def destroy
    @open_hour.destroy
    redirect_to open_hours_url, notice: 'Open hour was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_open_hour
    @open_hour = OpenHour.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def open_hour_params
    params.require(:open_hour).permit(:valid_from, :valid_until, :start_time, :end_time, :sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday)
  end

  def convert_to_utc(hours, minutes)
    t = Time.new(2000, 1, 1, hours, minutes, 0)
    t.utc
  end
end
