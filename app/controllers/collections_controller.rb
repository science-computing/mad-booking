class CollectionsController < ApplicationController
  before_action :set_collection, only: %i[show edit update destroy]
  before_action :authenticate
  before_action :authenticate_admin, except: [:hours_by_date]
  # GET /collections
  def index
    @collections = Collection.all
  end

  # GET /collections/1
  def show; end

  # GET /collections/new
  def new; end

  # GET /collections/1/edit
  def edit; end

  # POST /collections
  def create; end

  # PATCH/PUT /collections/1
  def update
    params[:collection][:settings].each do |key, value|
      @collection.settings(key.to_sym).update collection_params[:settings][key.to_sym]
    end
    if @collection.save
      redirect_to root_path, notice: "Update successful"
    else
      render action: 'edit'
    end
  end

  # DELETE /collections/1
  def destroy
    @setting.destroy
    redirect_to settings_url, notice: 'Setting was successfully destroyed.'
  end

  def hours_by_date
    test_date = Date.strptime(params[:date], '%m/%d/%Y')
    # Get open hours for a given date
    ot = OpenHour.open_times_from_date(test_date)
    ots = []
    ot.each do |open_time|
      # TODO: Support day of week!
      st_hr = open_time[0].localtime.hour
      st_min = open_time[0].localtime.min
      et_hr = open_time[1].localtime.hour
      et_min = open_time[1].localtime.min
      ots = { 'from' => [st_hr, st_min], 'to' => [et_hr, et_min] }
    end

    # Get any closed hours for a given date
    ct = ClosedHour.closed_times_from_date(test_date)
    cts = []
    ct.each do |closed_time|
      st_hr = closed_time[0].localtime.hour
      st_min = closed_time[0].localtime.min
      et_hr = closed_time[1].localtime.hour
      et_min = closed_time[1].localtime.min
      cts = { 'from' => [st_hr, st_min], 'to' => [et_hr, et_min] }
    end

    x = { open: ots, closed: cts }

    respond_to do |format|
      format.json { render json: x }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_collection
    @collection = Collection.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def collection_params
    params.require(:collection).permit(
      settings: [
        collection: [:name],
        self_serve: [:allow, :max_length, :enforce_dept],
        validation: [:watiam, :departments],
        email: [
          :from_address,
          :template_booking_from,
          :template_salutation,
          :on_sign_in,
          :on_sign_out,
          :on_booking,
          :on_approval,
          :on_rejection,
          :on_booking_edit,
          :on_booking_delete,
          :on_self_serve_booking,
          :on_self_serve_edit,
          :on_self_serve_delete
        ]
      ]
    )
  end
end
