# frozen_string_literal: true

include BookingsHelper
include EventsHelper

# BookingsController manages all of the logic surrounding the creating and
# editing of bookings and events
class BookingsController < ApplicationController
  helper :all
  helper_method :find_by_date_range

  before_action :authenticate
  before_action :authenticate_admin, except: %i[self_serve create update index edit show destroy] 
  before_action :authenticate_department, only: [:self_serve, :create, :update, :index, :edit, :show]

  before_action :check_self_serve, only: [:self_serve]

  def overdue
    overdue_bookings = get_overdue_items
    respond_to do |format|
      format.json { render json: overdue_bookings, include: %i[user equipment] }
    end
  end

  def upcoming_returns
    upcoming_bookings = find_upcoming_returns
    respond_to do |format|
      format.json { render json: upcoming_bookings, include: %i[user equipment] }
    end
  end

  def upcoming
    upcoming_bookings = find_upcoming_events(DateTime.now, 3)
    respond_to do |format|
      format.json { render json: upcoming_bookings, include: %i[user equipment] }
    end
  end

  def equipment_row
    @equip_id = params[:equip_id]
    @booking_exists = params[:booking_exists]
    @booking_id = params[:booking_id]

    respond_to do |format|
      format.js
    end
  end

  def column
    @bookings = Booking.pluck(request.params[:column])

    respond_to do |format|
      format.json { render json: @bookings }
    end
  end

  # GET /bookings
  # GET /bookings.json
  def index
    # Get logged in user
    logged_in_username = session[:current_username]
    user = get_or_create_user(logged_in_username, false)
    
    # If admin, get all bookings
    if user.admin?
      @bookings = Booking.all.paginate(page: params[:page], per_page: 10)

      # Handle user name search if present
      if params[:user_search].present?
        username = params[:user_search]
        @bookings = @bookings.joins(:user).where(users: { username: username })
      end
    else
      @bookings = Booking.all.joins(:user).where(users: {username: logged_in_username}).paginate(page: params[:page], per_page: 10)
    end

    # Handle sort order if present
    if params[:sort_order].present?
      sort_order = params[:sort_order]
      case sort_order
      when 'sd_on'
        @bookings = @bookings.joins(:events).order('events.start asc')
      when 'sd_no'
        @bookings = @bookings.joins(:events).order('events.start desc')
      when 'ed_on'
        @bookings = @bookings.joins(:events).order('events.end asc')
      when 'ed_no'
        @bookings = @bookings.joins(:events).order('events.end desc')
      end
    else
      @bookings = @bookings.joins(:events).order('events.start desc')
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bookings }
    end
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
    @booking = Booking.find(params[:id])
    username = session[:current_username]
    user = get_or_create_user(username, false)
    
    respond_to do |format|
      if user.admin?
        format.html # show.html.erb
        format.json { render json: @booking, include: %i[equipment user events] }
      elsif !user.admin? && @booking.user == user
        format.html 
        format.json { render json: @booking, include: %i[equipment user events] }
      else
        format.json { render json: {}, status: 401 }
      end
    end
  end

  # GET /bookings/new
  # GET /bookings/new.json
  def new
    @booking = Booking.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @booking }
    end
  end

  # GET /bookings/1/edit
  def edit
    @booking = Booking.find(params[:id])
  end

  # GET /bookings/1/self_serve
  def self_serve
    @booking = params[:id].nil? ? Booking.new : Booking.find(params[:id])
    respond_to do |format|
      # ensure bookings can only be edited in self serve view by their owner
      if @booking.user.nil?
        format.html
      else
        if @booking.user.username == session[:current_username]
          format.html
        else
          format.html { redirect_to selfserve_path, alert: "User attempting to edit someone else's booking" }
        end
      end
    end
  end

  # list pending approvals
  def approvals
    @bookings = Booking.where(approval_status: 0)

    respond_to do |format|
      format.html
      format.json { render json: @bookings, include: %i[user equipment] }
    end
  end

  # Respond to a piece of equipment being scanned
  # params should be the booking ID and the equipment ID
  def scan
    # Ensure the booking has the equipment attached
    equipment = Equipment.find(params[:equip_id])
    @booking = Booking.find(params[:id])
    current_eq_status = equipment.status
    # if current status is "In", toggle to "Out"
    # and record the item as signed out
    if current_eq_status == 1
      equipment.status = 0 
      @booking.sign_out_times[equipment.id] = DateTime.now
      new_label = "Signed out"
    elsif current_eq_status == 0
      equipment.status = 1 
      @booking.sign_in_times[equipment.id] = DateTime.now
      new_label = "Signed in"
    end
    if @booking.save && equipment.save
      if equipment.status == 0
        send_scan_out_notification([equipment.id])
      elsif equipment.status == 1
        send_scan_in_notification([equipment.id])
      end
      respond_to do |format|
        format.json { render json: { booking: @booking, new_label: new_label, status: :ok } }
      end
    end
  end

  # approve a booking (admin only)
  def approve
    @booking = Booking.find(params[:id])
    @booking.approval_status = 1
    @booking.approver = User.find_by_username(session[:current_username])
    if @booking.approver.admin
      respond_to do |format|
        if @booking.save
          send_self_serve_approval_notification
          format.json { render json: { booking: @booking }, status: :ok }
        else
          format.html { redirect_to root_path, alert: 'Something went wrong while saving: ' + @booking.errors.messages[:booking][0] }
        end
      end
    else
      respond_to do |format|
        format.html { redirect_to root_path, alert: 'User not allowed to approve bookings' }
      end
    end
  end

  # reject a booking (admin only)
  def reject
    @booking = Booking.find(params[:id])
    @booking.approval_status = 2
    @booking.approver = User.find_by_username(session[:current_username])

    respond_to do |format|
      if @booking.save
        send_self_serve_rejection_notification
        format.json { render json: { booking: @booking }, status: :ok }
      else
        format.html { redirect_to root_path, alert: 'Something went wrong while saving: ' + @booking.errors.messages[:booking][0] }
      end
    end
  end

  # POST /bookings
  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)
    # Get the key params from the request
    selfserve = params[:booking][:self_serve]

    # if this is a selfserve booking, username is in the session
    # otherwise it must be supplied as a param
    username = selfserve ? session[:current_username] : params[:booking][:username]
    equipment_list = params[:booking][:equipment]

    # get logged in user
    logged_in_user = User.find_by_username(session[:current_username])

    # get and set booking user
    user = get_or_create_user(username, true)
    # user = get_or_create_user(username, params[:create_if_new])
    @booking.user = user

    # set creator user
    @booking.creator = logged_in_user.username

    # set booking equipment
    @booking.equipment_ids = equipment_list

    # build booking schedule
    @booking.schedule = build_recurrence(
      params[:booking][:start_date_time],
      params[:booking][:end_date_time]
    )

    # if self serve set status to pending
    # otherwise set status to approved (assume admin created it)
    if !selfserve
      @booking.self_serve = false
      @booking.approval_status = 1
      @booking.approver = User.find_by_username(params[:booking][:creator])
    else
      @booking.self_serve = true
      @booking.approval_status = 0
    end
    # If user logged in is not an admin, approval status must be 0 on creation
    unless logged_in_user.nil?
      @booking.approval_status = 0 unless logged_in_user.admin
    end

    respond_to do |format|
      if @booking.save
        if selfserve
          # Send an email to the user informing them of the details of the booking
          send_self_serve_create_notification
          # if a self serve booking redirect to confirm page
          format.html { redirect_to selfserve_edit_path(@booking), notice: 'Booking was successfully created. You will receive email notification when it is approved.' }
          format.json { render json: @booking, status: :created, location: @booking }
        else
          # Send an email to the user informing them of the details of the booking
          send_admin_create_notification(format)
          # Go back to main page
          format.html { redirect_to root_path, notice: 'Booking was successfully created.' }
          format.json { render json: @booking, status: :created, location: @booking }
        end
      else
        if selfserve
          format.html { redirect_to selfserve_path, notice: format('Booking was not created: %s', @booking.errors.messages[:booking][0]) }
        else
          format.html { redirect_to root_path, alert: 'Something went wrong while saving: ' + @booking.errors.messages[:booking][0] }
          format.json { render json: { errors: @booking.errors }, status: :unprocessable_entity, location: @booking }
        end
      end
    end
  end



  # PUT /bookings/1
  # PUT /bookings/1.json
  def update
    # TODO: Doesn't handle self-serve differently
    # TODO: Doesn't email
    # TODO: Doesn't handle updating equipment status

    # New update method without anything related to 
    # approval or signing equipment in or out
    collection = Collection.first

    # Get the booking by ID
    @booking = Booking.find(params[:id])

    # Is this a self serve booking?
    selfserve = params[:booking][:self_serve]

    # if this is a selfserve booking, username is in the session
    # otherwise it must be supplied as a param
    username = selfserve ? session[:current_username] : params[:booking][:username]
    equipment_list = params[:booking][:equipment]

    # get logged in user
    logged_in_user = User.find_by_username(session[:current_username])

    # get and set booking user
    user = get_or_create_user(username, params[:create_if_new])
    @booking.user = user

    # set creator user
    @booking.creator = logged_in_user.username

    # set booking equipment
    @booking.equipment_ids = equipment_list

    # List old events in case of failure
    event_list_copy = []
    @booking.events.each do |ev|
      event_list_copy.push(Event.new(start: ev.start, end: ev.end))
    end

    # build booking schedule
    @booking.schedule = build_recurrence(
      params[:booking][:start_date_time],
      params[:booking][:end_date_time]
    )

    respond_to do |format|
      if @booking.update(booking_params)
        # send relevant email
        send_admin_edit_notification
        # booking saved succesfully
        format.html { redirect_to root_path, notice: 'Booking was successfully updated.' }
        format.json { render json: @booking, status: :ok, location: @booking }
      else
        # something went wrong saving the booking
        @booking.events = event_list_copy
        format.html { redirect_to root_path, alert: 'Something went wrong while saving: ' + @booking.errors.messages[:booking][0] }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    collection = Collection.first
    selfserve = if !params[:booking].nil?
                  params[:booking][:self_serve]
                else
                  false
                end
    @booking = Booking.find(params[:id])
    respond_to do |format|
      if selfserve
        send_self_serve_destroy_notification
      else
        send_admin_destroy_notification
      end
      @booking.destroy if @booking.sign_out_times.empty?

      format.html { redirect_to root_path }
      # format.json { head :no_content }
      format.json { render json: @booking, status: :ok, location: @booking }
    end
  end

  private

  # send an email notification whena  self serve booking is rejected
  def send_self_serve_rejection_notification()
    collection = Collection.first
    if collection.settings(:email).on_rejection == '1'
      begin
        UserMailer.admin_rejected_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to root_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # send an email notification when a self serve booking is approved
  def send_self_serve_approval_notification()
    collection = Collection.first
    if collection.settings(:email).on_approval == '1'
      begin
        UserMailer.admin_approved_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to root_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # send an email notification when a user deletes their own event
  def send_self_serve_destroy_notification()
    collection = Collection.first
    if collection.settings(:email).on_self_serve_delete == '1'
      begin
        UserMailer.self_delete_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to selfserve_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # send an email notification when a booking is created by an admin
  def send_admin_destroy_notification()
    collection = Collection.first
    if collection.settings(:email).on_booking_delete == '1'
      begin
        UserMailer.admin_delete_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to root_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # Send an email notification when an item is scanned out
  def send_scan_out_notification(eq_ids)
    collection = Collection.first
    if collection.settings(:email).on_sign_out == "1"
      begin
        UserMailer.admin_sign_out_email(@booking.user, @booking, eq_ids).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to selfserve_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  
  # Send an email notification when an item is scanned in
  def send_scan_in_notification(eq_ids)
    collection = Collection.first
    if collection.settings(:email).on_sign_in == "1"
      begin
        UserMailer.admin_sign_in_email(@booking.user, @booking, eq_ids).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to selfserve_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # Send an email notification for a self serve create event
  def send_self_serve_create_notification()
    collection = Collection.first
    if collection.settings(:email).on_self_serve_booking == "1"
      begin
        UserMailer.self_create_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to selfserve_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # Send an email notification for an admin creating an event
  def send_admin_create_notification(format)
    collection = Collection.first
    if collection.settings(:email).on_booking == "1"
      begin
        UserMailer.admin_create_email(@booking.user, @booking).deliver
      rescue OpenSSL::SSL::SSLError, Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to root_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # Send an email notification for an admin editing the event
  def send_admin_edit_notification()
    collection = Collection.first
    if collection.settings(:email).on_booking_edit == '1'
      begin
        UserMailer.admin_edit_email(@booking.user, @booking).deliver
      rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
        format.html { redirect_to root_path, notice: format('An error occurred sending confirmation email: %s ', e) }
      end
    end
  end

  # Retrieve (and if necessary, create) a user object
  def get_or_create_user(username, create_if_new)

    # Find the user based on the provided username
    user = User.find_by_username(username)

    # If the user doesn't exist and we are told to create them, then create them
    if user.nil? && create_if_new
      User.create(username: username)
      user = User.find_by_username(username)
    end

    return user
  end

  def booking_layout
    params[:action] == 'edit' || params[:action] == 'new' ? false : 'application'
  end

  def booking_params
    params.require(:booking).permit(:self_serve)
  end

  def check_user_exists
    uname = params[:booking][:user]
    return true unless User.where(username: uname).empty?

    respond_to do |format|
      format.html do
        flash[:notice] = 'User does not exist: ' + uname
        render action: 'new'
      end
    end
  end

  def build_recurrence(event_start_dt, event_end_dt)
    # event_start_dt = Time.strptime(event_start_d + event_start_t, "%Y-%m-%d%H:%M").utc;
    # event_end_dt = Time.strptime(event_end_d + event_end_t, "%Y-%m-%d%H:%M").utc;
    weekly_rep = params[:booking][:weekly_repeat]
    num_weeks = params[:booking][:num_weeks]
    old_ev_ids = @booking.events.ids.dup

    @booking.events = []
    if weekly_rep == true
      # TODO: This probably isn't smart, but it'll work for now
      # This will be a problem for history tracking
      r = Recurrence.new(
        every: :week, 
        on: DateTime.parse(event_start_dt).strftime('%A').parameterize.underscore.to_sym,
        repeat: num_weeks.to_i,
        starts: event_start_dt.to_date
      )

      r.events.each_with_index do |date, index|
        # Parse the start and end dt strings into new DateTime objects
        # and add the number of weeks (we are making an assumption that
        # recurrence is always weekly with no gaps)
        start_dt = DateTime.parse(event_start_dt) + index.week
        end_dt = DateTime.parse(event_end_dt) + index.week
        
        @booking.events.build(start: start_dt, end: end_dt)
      end
    else
      # a single event, no recurrence pattern specified
      @booking.events.build(start: event_start_dt, end: event_end_dt)
      r = nil
    end
    r
  end
end
