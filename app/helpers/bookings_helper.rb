# frozen_string_literal: true

require 'benchmark'
# BookingsHelper contains helper methods for retreiving events that meet
# specific criteria
module BookingsHelper
  # Returns a list of active (not overdue)events that have their start date
  # before the supplied start date
  #
  def find_upcoming_returns
    range_bookings_return = []
    events = Event.includes(booking: %i[user equipment]).where('start <= ? AND "end" >= ?', Time.now, Time.now).order('"end"')
    events.each do |ev|
      range_bookings_return.push(ev.booking) if ev.booking.is_active?
    end
    range_bookings_return
  end

  # Return a list of overdue bookings, sorted by event end date
  def get_overdue_items
    overdue_bookings = []
    # get all events with an end date within the last year
    # events_before_now = Event.includes(booking: %i[user equipment]).where('"end" <= ?', Time.now).order('"end"')
    events_before_now = Event.includes(booking: %i[user equipment]).where('"end" >= ?', Time.now - 1.year).order('"end"')
    events_before_now.each do |event|
      overdue_bookings.push(event.booking) if event.booking.is_overdue?
    end

    overdue_bookings
  end
end
