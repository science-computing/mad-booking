# frozen_string_literal: true

class ChangeEventBookingColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :events, :parent_booking_id, :booking_id
  end
end
