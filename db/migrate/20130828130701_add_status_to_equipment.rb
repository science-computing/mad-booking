# frozen_string_literal: true

class AddStatusToEquipment < ActiveRecord::Migration[5.1]
  def change
    add_column :equipment, :status, :integer
  end
end
