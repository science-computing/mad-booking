# frozen_string_literal: true

class CreateOpenHours < ActiveRecord::Migration[5.1]
  def change
    create_table :open_hours do |t|
      t.date :valid_from
      t.date :valid_until
      t.time :start_time
      t.time :end_time
      t.boolean :sunday
      t.boolean :monday
      t.boolean :tuesday
      t.boolean :wednesday
      t.boolean :thursday
      t.boolean :friday
      t.boolean :saturday

      t.timestamps
    end
  end
end
