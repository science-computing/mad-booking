# frozen_string_literal: true

class AddNotNullConstraints < ActiveRecord::Migration[5.1]
  def change
    change_column_null(:events, :created_at, false)
    change_column_null(:events, :updated_at, false)
  end
end
