# frozen_string_literal: true

class CreateClosedHours < ActiveRecord::Migration[5.1]
  def change
    create_table :closed_hours do |t|
      t.datetime :closed_start
      t.datetime :closed_end

      t.timestamps
    end
  end
end
