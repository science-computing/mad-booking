# frozen_string_literal: true

# call this file in the rails console by
#    load "filename.rb"
# then simply call the below functions

def settings_validator
  # Ensure required settings are created and have necessary values
  if Setting.find_by_key('email_on_booking').nil?
    Setting.create(key: 'email_on_booking', value: false)
  end
  if Setting.find_by_key('email_on_sign_out').nil?
    Setting.create(key: 'email_on_sign_out', value: false)
  end
  if Setting.find_by_key('email_on_sign_in').nil?
    Setting.create(key: 'email_on_sign_in', value: false)
  end
  if Setting.find_by_key('email_from_address').nil?
    Setting.create(key: 'email_from_address', value: 'nobody@nobody.com')
  end
  if Setting.find_by_key('validate_watiam').nil?
    Setting.create(key: 'validate_watiam', value: false)
  end
  if Setting.find_by_key('allow_self_serve').nil?
    Setting.create(key: 'allow_self_serve', value: false)
  end
  if Setting.find_by_key('site_name').nil?
    Setting.create(key: 'site_name', value: 'Temporary name')
  end
end

def username_validator
  all_users = User.all
  all_users.each do |user|
    # Trim to max 8 characters
    if user.username.length > 8
      oldname = user.username
      newname = user.username[0, 8]
      user.username = newname
      user.save!
      puts format('%s was more than 8 characters. It was truncated to %s', oldname, user.username)
    end

    # Check for spaces
    if user.username.include? ' '
      user.username.delete! ' '
      user.save!
      puts format('%s had spaces. These were removed.', user.username)
    end

    # Check for watiam validity
    next if User.ldap_user?(user.username)

    user.status = 2
    user.comments = 'Blacklisted because the username is not a known WatIAM username'
    user.save!
    puts format('%s is not a valid WatIAM user and has been blacklisted', user.username.strip)
  end
end

def booking_converter
  all_bookings = Booking.all

  all_bookings.each do |booking|
    next if booking.schedule.nil?

    e = booking.events.build(start: booking.schedule[:start_date], end: booking.schedule[:end_time], booking_id: booking.id)
    e.save
    booking.update_attribute(:schedule, nil)
    booking.save
  end
end
