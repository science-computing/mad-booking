#!/bin/sh

docker build -t git.uwaterloo.ca:5050/mad/mad-booking/madbooking_app_prod -f app.prod.Dockerfile .
docker build -t git.uwaterloo.ca:5050/mad/mad-booking/madbooking_apache_prod -f apache.prod.Dockerfile .