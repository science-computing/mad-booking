#! /bin/bash
git fetch upstream
git rebase upstream/master
docker build -t git.uwaterloo.ca:5050/science-computing/mad-booking -f app.prod.Dockerfile .
docker push git.uwaterloo.ca:5050/science-computing/mad-booking

