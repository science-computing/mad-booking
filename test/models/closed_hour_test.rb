# frozen_string_literal: true

require 'test_helper'
require 'date'

class ClosedHourTest < ActiveSupport::TestCase
  test 'should not save ClosedHour without start and end' do
    ch = ClosedHour.new
    assert_not ch.save
  end

  test 'should not save ClosedHour without start' do
    ch = ClosedHour.new(closed_end: Date.today)
    assert_not ch.save
  end

  test 'should not save ClosedHour without end' do
    ch = ClosedHour.new(closed_start: Date.today)
    assert_not ch.save
  end

  test 'should not save ClosedHour when end is before start' do
    ch = ClosedHour.new(
      closed_start: Date.today,
      closed_end: Date.today - 1.day
    )
    assert_not ch.save
  end

  test 'closed_times_from_date should return appropriate ClosedHours' do
    cts = ClosedHour.closed_times_from_date(Date.new(2019, 10, 31))
    assert cts.length == 2

    cts = ClosedHour.closed_times_from_date(Date.new(2018, 1, 1))
    assert cts.empty?
    assert_equal cts, []
  end
end
