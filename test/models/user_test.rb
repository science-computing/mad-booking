# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should not save user without a name' do
    u = User.new
    assert_not u.save
  end

  test 'should not save User without a unique name' do
    u = User.new(username: 'astudent')
    assert_not u.save
  end

  test 'admin_user? should return appropriately for known users' do
    assert User.admin_user?(users(:user_admin).username)
    assert_not User.admin_user?(users(:user_regular).username)
  end

  test 'admin_user? should return false for unknown users' do
    assert_not User.admin_user?('bob')
  end

  test 'users should default to non-admins and good standing' do
    u = User.new(username: 'tester')
    assert u.save
    assert u.admin == false
    assert u.status.zero?
  end

  test 'Users.valid_statuses only has 3 items' do
    assert User.valid_statuses.length == 3
  end

  test 'Users.get_status returns appropriate values' do
    assert_equal User.get_status(0), 'Good Standing'
    assert_equal User.get_status(1), 'Warned'
    assert_equal User.get_status(2), 'Blacklisted'
  end

  test 'find_or_create_from_username returns or creates a user' do
    # Return existing user
    u = User.find_or_create_from_username('anadmin')
    assert_equal true, u.valid?

    # Create new user
    u = User.find_or_create_from_username('fakeuser')
    assert_equal true, u.valid?

    u.delete
  end

  test 'watiam_user? returns true/false appropriately based on username' do
    # WatIAM setting is true
    s = Setting.create(
      key: 'validate_watiam',
      value: 'true'
    )

    assert_equal true, User.watiam_user?('jmccarth')
    assert_equal false, User.watiam_user?('zzzzzzzz')

    s.delete
  end

  test 'watiam_user? returns true/false appropriately considering settings' do
    # WatIAM validation happens by default, unless there is an explicit
    # false setting
    assert_equal true, User.watiam_user?('jmccarth')
    assert_equal false, User.watiam_user?('zzzzzzzz')

    s = Setting.create(
      key: 'validate_watiam',
      value: 'false'
    )

    assert_equal true, User.watiam_user?('jmccarth')
    assert_equal true, User.watiam_user?('zzzzzzzz')

    s.delete
  end

  test 'watiam_details returns a JSON response for a real username' do
    u = User.create(username: 'jmccarth')
    response = u.watiam_details
    assert_equal Hash, response.class
    assert_equal 200, u.watiam_details['meta']['status']
    u.delete
  end

  test 'watiam_details returns a usable response for incorrect username' do
    u = User.create(username: 'zzzzzzzz')
    response = u.watiam_details
    assert_equal Hash, response.class
    assert_equal 204, u.watiam_details['meta']['status']
    u.delete
  end

  test 'validate_department returns true if valid dept based on settings' do
    u = User.create(username: 'jmccarth')

    assert_equal false, u.validate_department

    s = Setting.create(
      key: 'valid_departments',
      value: 'Mapping, Analysis & Design | Some other department'
    )

    assert_equal true, u.validate_department

    s[:value] = 'Another department | Some other department'
    s.save
    assert_equal false, u.validate_department

    s.delete
    u.delete
  end

  test 'email_address returns an appropriate email for a real user' do
    u = User.create(username: 'jmccarth')

    assert_equal 'jmccarth@uwaterloo.ca', u.email_address

    u.delete
  end

  test 'email_address returns appropriately if user not found' do
    u = User.create(username: 'zzzzzzzz')

    assert_nil u.email_address

    u.delete
  end
end
