# frozen_string_literal: true

require 'test_helper'

class TagTest < ActiveSupport::TestCase
  test 'should not save Tag without a name' do
    t = Tag.new
    assert_not t.save
  end

  test 'should not save Setting without a unique name' do
    t = Tag.new(name: 'the tag')
    assert_not t.save
  end
end
