# frozen_string_literal: true

Rails.application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    get '/auth/create', to: 'sessions#create', as: 'auth_create'
    get '/auth/logout', to: 'sessions#destroy', as: 'auth_logout'

    resources :collections
    resources :closed_hours
    resources :open_hours
    resources :tags

    resources :settings do
      collection do
        put :update
        patch :update
      end
    end

    get '/invaliduser', to: 'pages#invaliduser', as: 'invalid_user'
    get '/selfserve', to: 'bookings#self_serve', as: 'selfserve'
    get '/selfserve_disabled', to: 'pages#selfserve_disabled', as: 'selfserve_disabled'
    get '/selfserve_success', to: 'pages#selfserve_success', as: 'selfserve_success'
    get '/selfserve_fail', to: 'pages#selfserve_fail', as: 'selfserve_fail'
    get '/approvals', to: 'bookings#approvals', as: 'approvals'
    get '/bookings/:id/reject', to: 'bookings#reject', as: 'reject', defaults: { format: :json }
    get '/bookings/:id/approve', to: 'bookings#approve', as: 'approve', defaults: { format: :json }
    post '/bookings/:id/scan', to: 'bookings#scan', as: 'scan', defaults: { format: :json }
    get '/user_history', to: 'pages#user_history', as: 'user_history'
    get '/equipment/download', to: 'equipment#download_excel'
    get '/equipment/unavailable', to: 'equipment#unavailable_time_range', defaults: { format: :json }
    get '/equipment/:id/summary_view', to: 'equipment#summary_view', as: 'equipment_summary_view'
    get '/tags/form/:tagid', to: 'tags#form', as: 'tag_form' # generate a partial to edit existing forms
    get '/users/form/:userid', to: 'users#form', as: 'user_form' # generate a partial to edit existing forms
    get '/equipment/form/:equip_id', to: 'equipment#form', as: 'equipment_form' # generate a partial to edit existing forms
    get '/bookings/:id/self_serve', to: 'bookings#self_serve', as: 'selfserve_edit'
    get '/bookings/equiprow/:equip_id', to: 'bookings#equipment_row', as: 'equiprow' # generate the partial js for a bookings row
    get '/bookings/overdue', to: 'bookings#overdue', defaults: { format: :json }
    get '/bookings/upcoming', to: 'bookings#upcoming', defaults: { format: :json }
    get '/bookings/upcoming_returns', to: 'bookings#upcoming_returns', defaults: { format: :json }
    get '/bookings/calendar', to: 'pages#calendar_new'
    get '/bookings/approvals', to: 'bookings#approvals', defaults: { format: :json }
    get '/users/validate_dept/:userid.json', to: 'users#validate_dept', defaults: { format: :json } # get whether a user is from a valid department
    get '/users/ldap/:userid.json', to: 'users#ldap', defaults: { format: :json } # get LDAP details for a user
    get '/users/ldap_user/:username.json', to: 'users#valid_ldap_user', defaults: { format: :json } # determine if a username exists in LDAP
    get '/users/:column.json', to: 'users#column', defaults: { format: :json }, constraints: ->(request) { User.column_names.include?(request.params[:column]) }
    get '/users/ldap_search/', to: 'users#ldap_search', defaults: { format: :json }
    get '/equipment/bytag/', to: 'equipment#tags', as: 'bytag'
    get '/equipment/:column.json', to: 'equipment#column', defaults: { format: :json }, constraints: ->(request) { Equipment.column_names.include?(request.params[:column]) }
    get '/hours_by_date', to: 'settings#hours_by_date', as: 'hours_by_date', defaults: { format: :json }
    get '/events/calendar', to: 'events#calendar', defaults: { format: :json }
    get '/equipment/:id/future_events', to: 'equipment#future_events', defaults: { format: :json }

    resources :equipment
    resources :users
    resources :bookings
    resources :events

    root to: 'pages#home2'
  end
end
