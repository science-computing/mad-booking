# frozen_string_literal: true

ActiveRecordQueryTrace.enabled = false
ActiveRecordQueryTrace.level = :app
