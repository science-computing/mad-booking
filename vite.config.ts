import { defineConfig } from 'vite'
import RubyPlugin from 'vite-plugin-ruby'
// import vue from  '@vitejs/plugin-vue'
import { createVuePlugin } from 'vite-plugin-vue2'

export default defineConfig({
  plugins: [
    RubyPlugin(),
    // vue(),
    createVuePlugin()
  ],
  // base: '/mad-booking/vite-dev/'
})
